
# Step project Cards

Cards is a responsive website for creating doctor appointments.

## Used technologies

- HTML
- SCSS
- JS/ jQuery
- Bootstrap
- Gulp



## Developers and their tasks

- [Alyona kolgasheva](https://gitlab.com/Kolgasjeva)
  - Create GitLab repository with necessary files.
  - Modal for creating appointments.
  - Displaying the created card on the page.
- [Daria Vasylenko](https://gitlab.com/DARIA96)
  - Editing the card.
  - Removal the card from the page.
- [Alina Tkachenko](https://gitlab.com/AlinaTkachenko)
  - Header and authorization .
  - Filter section.
- Shared tasks:
  - Code review of each other.
  - Fixing bugs.
  - Publishing the website on GitHub Pages.

