import {createVisitFabric} from "./createCard";


export class FormManager {
    constructor() {
        this.selectDoctors = document.getElementById('doctors');
        this.optionalFields = document.querySelectorAll('.optional-fields');
        this.btnClose = document.querySelector('btn-close');
        this.btnVisit = document.querySelector('.create-visit'); 
        this.wrapperForm = document.querySelector('.wrapper-form');
        this.optionalInputs = document.querySelectorAll('.optional-field');
        this.form = document.querySelector('.form-cards');
    }

    //init all events
    init() {
        this.btnVisit.addEventListener('click', () => {
            this.openNew();
        })

        this.wrapperForm.addEventListener('click', (event) => {
            if (event.target.classList.contains("wrapper-form") ||
                event.target.classList.contains("btn-close") ||
                event.target.classList.contains("fa-xmark")) {
                this.close();
            }
        });

        this.selectDoctors.addEventListener('change', (value) => {
            this.visualizeDoctorFields(value.target.value);
        })

        return this;
    }

    openForEdit(cardId) {
        this.clearForm('Оновити').loadFromServer(cardId).show();
       return this;
    } 

    openNew() {
        this.clearForm().show();
        return this;
    }


    show() {
        this.wrapperForm.classList.remove('wrapper-form--display');
        return this;
    }

    close() {
        this.wrapperForm.classList.add('wrapper-form--display');
        return this;
    }

    clearForm(text = 'Створити') {
        this.form.reset();
        document.getElementById('form_btn_create').innerHTML = text;

        return this;
    }


    item;

    loadFromServer(cardId) {
       this.item = localStorage.getItem('token');
       const token = this.item;
        fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${token}`
            },
        })
            .then(data => data.json())
            .then(data => {
                if (data.id > 0) {
                    const cardDoctor = createVisitFabric(data)
                    this.writeToInputs(cardDoctor).visualizeDoctorFields(cardDoctor.doctors);
                }
            })

        return this;
    }

    writeToSelectElement(id, valueToSelect) {
        let element = document.getElementById(id);
        element.value = valueToSelect;
    }


    writeToInputs(data) {
        document.getElementById('form_cards_id').value = data.cardId
        document.getElementById('purpose').value = data.purpose === undefined ? '' : data.purpose;
        document.getElementById('description').value = data.description === undefined ? '' : data.description;
        document.getElementById('fullName').value = data.fullName === undefined ? '' : data.fullName;
        document.getElementById('bloodPressure').value = data.bloodPressure === undefined ? '' : data.bloodPressure;
        document.getElementById('heartDiseases').value = data.heartDiseases === undefined ? '' : data.heartDiseases;
        document.getElementById('bmi').value = data.bmi === undefined ? '' : data.bmi;
        document.getElementById('age').value = data.age === undefined ? '' : data.age;
        document.getElementById('lastVisitDate').value = data.lastVisitDate === undefined ? '' : data.lastVisitDate;
        document.getElementById('years').value = data.years === undefined ? '' : data.years;

        this.writeToSelectElement('doctors', (data.doctors === undefined ? '' : data.doctors) );
        this.writeToSelectElement('urgency', (data.urgency === undefined ? '' : data.urgency ));
        this.writeToSelectElement('status', (data.status === undefined ? '' : data.status ));

        return this;
    }


    visualizeDoctorFields(value) {
        this.optionalFields.forEach(field => {
            field.classList.add('optional-fields--display');
        })
        this.optionalInputs.forEach(input => {
            input.removeAttribute('required');
        })
        const chosenDoctor = document.querySelector(`[data-doctor='${value}']`);
        chosenDoctor.classList.remove('optional-fields--display');
        const doctorsInputs = document.querySelectorAll(`.${value}-field`);
        doctorsInputs.forEach(input => {
            input.setAttribute('required', 'true');
        })

        return this;
    }


}

const formManager = new FormManager();
formManager.init();
