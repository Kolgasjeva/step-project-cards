import {cardGridOperation} from './createCard.js';
const purpose = document.getElementById('Purpose');
const description = document.getElementById('Description');
const status = document.getElementById('status');
const urgency = document.getElementById('urgency');

let cards = localStorage.cards && JSON.parse(localStorage.cards) || [];
console.log(cards)

purpose.addEventListener('input', event => {
    onChange();
});

description.addEventListener('input', event => {
    onChange();
});

status.addEventListener('change', event => {
    onChange();
});

urgency.addEventListener('change', event => {
    onChange();
});

function onChange() {
    const filteredCards = cards.filter(card => {
        return card.purpose.includes(purpose.value) && card.description.includes(description.value) && card.status.includes(status.value)  && card.urgency.includes(urgency.value) 
    })
    cardGridOperation.renderCardBlock(filteredCards);
}

export function isEmptyFilter() {
    return purpose.value === '' && description.value === '' && status.value === '' && urgency.value === ''
}