import {FormManager} from "./modalBox";
import {isEmptyFilter} from "./filter";

const form = document.querySelector('.form-cards');
const formInput = document.querySelectorAll('.form-control');
const formSelect = document.querySelectorAll('.form-select');
const token = localStorage.getItem('token');
const formData = {};
const wrapperForm = document.querySelector('.wrapper-form');
const filter = document.querySelector('.filter');

export class Visit {
    constructor(obj) {
        this.doctors = obj.doctors;
        this.purpose = obj.purpose;
        this.description = obj.description;
        this.urgency = obj.urgency;
        this.fullName = obj.fullName;
        this.containerCard = document.createElement('div');
        this.cardId = obj.id;
        this.status = obj.status;
    }

    renderPartialHTMLCard(additionalHtml){
        const statusColor = this.status === 'Новий візит' ? 'new' : '';
        return `
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 pb-2 card-visit card g-3" data-id="${this.cardId}" id="card_block_id_${this.cardId}"> 
            <div class="card-header">Статус: <span class="${statusColor}">${this.status}</span></div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item">ПІБ: ${this.fullName}</li>
                    <li class="list-group-item">Лікар: ${this.doctors}</li>
                </ul>
            </div>
            <div class="collapse mb-3" id="collapseExample_${this.cardId}">
                <div class="card card-body">
                    <ul class="list-group">
                        <li class="list-group-item">Мета візиту: ${this.purpose}</li>
                        <li class="list-group-item">Короткий опис візиту: ${this.description}</li>
                        <li class="list-group-item"> Терміновість: ${this.urgency}</li>
                    </ul> 
                    <p class="card__additional-title">Додаткова інформація:</p>
                    <ul class="list-group list-group"> ${additionalHtml}</ul>
                </div>
            </div>
            
            <button class="btn-close card-btn-close text-right" data-id="${this.cardId}"></button>
         <div class="ps-4">
             <button class="btn btn-primary btn-details" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample_${this.cardId}" aria-expanded="false" aria-controls="collapseExample_${this.cardId}">
                Показати більше
            </button>
            <button class="btn btn-secondary btn-edit" data-id="${this.cardId}">Редагувати</button>
         </div>
        </div>`
    }
}

class VisitCardiologist extends Visit {
    constructor(obj) {
        super(obj);
        this.bloodPressure = obj.bloodPressure;
        this.bmi = obj.bmi;
        this.heartDiseases = obj.heartDiseases;
        this.age = obj.age;
    }

    renderPartialHTMLCard(){
        let additionalHtml = `
        <li class="list-group-item">Звичайний тиск: ${this.bloodPressure}</li>
        <li class="list-group-item">Індекс маси тіла: ${this.bmi}</li>
        <li class="list-group-item">Перенесені захворювання серцево-судинної системи: ${this.heartDiseases}</li>
        <li class="list-group-item">Вік: ${this.age}</li>`       ;
        return super.renderPartialHTMLCard(additionalHtml);
    }
}

class VisitDentist extends Visit {
    constructor(obj) {
        super(obj);
        this.lastVisitDate = obj.lastVisitDate;
    }

    renderPartialHTMLCard(){
        let additionalHtml = `
        <li class="list-group-item">Дата останнього відвідування: ${this.lastVisitDate}</li>`       ;
        return super.renderPartialHTMLCard(additionalHtml);
    }
}

class VisitTherapist extends Visit {
    constructor(obj) {
        super(obj);
        this.years = obj.years;
    }

    renderPartialHTMLCard(){
        let additionalHtml = `
        <li class="list-group-item">Вік: ${this.years}</li>`       ;
        return super.renderPartialHTMLCard(additionalHtml);
    }
}


class CardGridOperation {
    getToken(){
        //todo дописати повторну автоізациюб якщо немає токена
        const token = localStorage.getItem('token');
        if(!token){
            alert("Ви не авторізовані!") //для настрою
        }
        return token;
    }

    // reload grid with filter
    reLoadGrid() {
        //todo add filter params here

        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.getToken()}`
            }
        })
            .then(response => response.json())
            .then(cards => {
                    this.renderCardBlock(cards)
                    localStorage.cards = JSON.stringify(cards);
                }
            )
    }

    deleteCard(cardId) {
        fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.getToken()}`
            },
        })
            .then(res => {
                if (res.ok) {
                    filterCards.reLoadGrid();

                } else {
                    console.log('Помилка при видаленні карти');
                }
            })
            .catch(error => {
                console.log('Помилка при виконанні запиту', error);
            });
    }

    editCard(cardId) {
        const form = new FormManager();
        form.openForEdit(cardId)
    }

    // render cards in general grid card
    renderCardBlock(cards) {
        let textHTML = '';

        if(isEmptyObject(cards) && isEmptyFilter()){
            textHTML = `<span id="no_cards">No items have been found</span>`;
            filter.classList.remove('d-block');
            filter.classList.add('d-none');
        }else {
            cards.forEach(item => {
                const card = createVisitFabric(item)
                textHTML += card.renderPartialHTMLCard();
                filter.classList.add('d-block');
                filter.classList.remove('d-none');
            })
        }
    
        const containerVisits = document.querySelector('.containers-visits');
        containerVisits.innerHTML = textHTML;
    }

    // init all event related card operation: edit, delete
    init(){
        const bodyElement = $('body');

        bodyElement.on('click', '.card-btn-close', function (e){
            const id = $(this).data('id');

            const filterCards = new CardGridOperation();
            filterCards.deleteCard(id);
        });

        bodyElement.on('click', '.btn-edit', function (e){
            const id = $(this).data('id');

            const filterCards = new CardGridOperation();
            filterCards.editCard(id);
        });

        bodyElement.on('click', '.btn-details', function (e){
            let button = $(this);
            let initialText = 'Показати більше';
            let newText = 'Приховати деталі';
            
            if (button.text() === newText) {
                button.text(initialText);
            } else {
                button.text(newText);
            }
        });

        //todo переписати на нову логіку
        bodyElement.on('click', '.btn-details', function (e){
            //this.commonFields.classList.remove('card-visit--display');
            //                 this.additionalFields.classList.remove('card-visit--display');
            //                 btnViewDetails.classList.add('btn-details--display');
            //                 btnCloseDetails.classList.remove('btn-details--display')
        });

        //todo переписати на нову логіку
        bodyElement.on('click', '.btn-details', function (e){
            // this.commonFields.classList.remove('card-visit--display');
            //                 this.additionalFields.classList.remove('card-visit--display');
            //                 btnViewDetails.classList.add('btn-details--display');
            //                 btnCloseDetails.classList.remove('btn-details--display')
        });


        // load cards in grid when open site
        filterCards.reLoadGrid();
    }
}

export const cardGridOperation = new CardGridOperation();

const filterCards = new CardGridOperation();
filterCards.init();

function isEmptyObject(obj) {
    return Object.keys(obj).length === 0;
}

export function createVisitFabric(obj) {
    let newVisit = {};
    switch (obj.doctors) {
        case "Кардіолог":
            newVisit = new VisitCardiologist(obj);
            break;
        case "Стоматолог":
            newVisit = new VisitDentist(obj);
            break;
        case "Терапевт":
            newVisit = new VisitTherapist(obj);
            break;
        default:
            break;
    }
    return newVisit;
}

window.addEventListener("load", () => {
    function sendData() {
        const cardId = document.getElementById('form_cards_id').value;

        if (doctors.value !== 'Не обрано') {
            formInput.forEach(input => {
                const dataKey = input.dataset.key;
                if (dataKey) {
                    formData[dataKey] = input.value;
                }
            })
            formSelect.forEach(select => {
                const dataKey = select.dataset.key;
                if (dataKey) {
                    let selectedOption = select.options[select.selectedIndex];
                    let selectedText = selectedOption.innerText;
                    formData[dataKey] = selectedText;
                }
            })

            if (cardId.length > 0) {
                fetch("https://ajax.test-danit.com/api/v2/cards/" + cardId, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(formData)
                }).then((data) => {
                    filterCards.reLoadGrid();
                });

            } else {
                fetch("https://ajax.test-danit.com/api/v2/cards", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(formData)
                }).then((data) => {
                    filterCards.reLoadGrid();
                });
            }
            wrapperForm.classList.add('wrapper-form--display')
        }
    }


    // Add 'submit' event handler
    $(document).ready(function() {
        $('form').submit(function(event) {
            event.preventDefault();
            sendData();
        });
    });
});
