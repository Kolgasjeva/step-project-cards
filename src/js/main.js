import {cardGridOperation} from './createCard.js';

const entry = document.querySelector('.entry');
const exit = document.querySelector('.exit');
const modalEntry = document.querySelector('.modal-entry');
const createVisit = document.querySelector('.create-visit');
const modal = document.querySelector('#entryModal');
const inputEmail = document.querySelector('#inputEmail');
const inputPassword = document.querySelector('#inputPassword');
const containerVisits = document.querySelector('.containers-visits');

if(!localStorage.token) {
  containerVisits.innerHTML = `<span id="no_cards">No items have been found</span>`;
}else {
  entry.style.display = 'none';
  exit.style.display = 'block';
  createVisit.style.display = 'block';
  containerVisits.innerHTML = ``;
}

modalEntry.addEventListener('click', event => {
  getToken(inputEmail.value, inputPassword.value);
})

exit.addEventListener('click', event => {
  localStorage.removeItem('token');
  entry.style.display = 'block';
  exit.style.display = 'none';
  createVisit.style.display = 'none';
  
  cardGridOperation.renderCardBlock([]);
})

async function getToken(email, password) {

  let response = await fetch('https://ajax.test-danit.com/api/v2/cards/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: `${inputEmail.value}`,
      password: `${inputPassword.value}`
    })
  })

  if (response.ok) {
    let token = await response.text();
    localStorage.setItem('token', token);

    inputEmail.classList.remove('is-invalid');
    inputPassword.classList.remove('is-invalid');
    entry.style.display = 'none';
    exit.style.display = 'block';
    createVisit.style.display = 'block';

    const modalInstance = bootstrap.Modal.getInstance(modal);
    modalInstance.hide();

    containerVisits.innerHTML = ``;

    cardGridOperation.reLoadGrid();

  } else {
    inputEmail.classList.add('is-invalid');
    inputPassword.classList.add('is-invalid');
  }
}

